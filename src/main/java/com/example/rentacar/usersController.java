package com.example.rentacar;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

@RestController
@RequestMapping("/users")
public class usersController {
    @Autowired
    databaseService databaseService;

    @RequestMapping(value = "/", method = RequestMethod.POST, consumes = {"application/json","application/xml"})
    @ResponseBody
    public ResponseEntity<Users> addUsers(@RequestBody Users users) {
        try {
            return new ResponseEntity<>(
                    databaseService.insertUsers(users),
                    HttpStatus.OK);
        }
        catch(org.springframework.http.converter.HttpMessageNotReadableException e)
        {
            throw new ResponseStatusException(
                    HttpStatus.BAD_REQUEST, "Zawartość nie może być pusta", e);
        }
        catch(java.util.NoSuchElementException e)
        {
            throw new ResponseStatusException(
                    HttpStatus.NOT_FOUND, "Nie znaleziono zasobu", e);
        }
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.PUT, consumes = {"application/json","application/xml"})
    @ResponseBody
    public ResponseEntity<Users> updateUsers(@PathVariable("id") Integer id, @RequestBody Users users) {
        try {
            return new ResponseEntity<>(
                    databaseService.updateUsers(id, users),
                    HttpStatus.OK);
        }
        catch(org.springframework.http.converter.HttpMessageNotReadableException e)
        {
            throw new ResponseStatusException(
                    HttpStatus.BAD_REQUEST, "Zawartość nie może być pusta", e);
        }
        catch(java.util.NoSuchElementException e)
        {
            throw new ResponseStatusException(
                    HttpStatus.NOT_FOUND, "Nie znaleziono zasobu", e);
        }
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    @ResponseBody
    public ResponseEntity<Users> deleteUsers(@PathVariable("id") Integer id) {
        try {
            return new ResponseEntity<>(
                    databaseService.deleteUsers(id),
                    HttpStatus.OK);
        }
        catch(org.springframework.http.converter.HttpMessageNotReadableException e)
        {
            throw new ResponseStatusException(
                    HttpStatus.BAD_REQUEST, "Zawartość nie może być pusta", e);
        }
        catch(java.util.NoSuchElementException e)
        {
            throw new ResponseStatusException(
                    HttpStatus.NOT_FOUND, "Nie znaleziono zasobu", e);
        }
    }

    @RequestMapping(value = "/", method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<List<Users>> getUsers(){
        try {
            return new ResponseEntity<>(
                    databaseService.getAllUsers(),
                    HttpStatus.OK);
        }
        catch(org.springframework.http.converter.HttpMessageNotReadableException e)
        {
            throw new ResponseStatusException(
                    HttpStatus.BAD_REQUEST, "Zawartość nie może być pusta", e);
        }
        catch(java.util.NoSuchElementException e)
        {
            throw new ResponseStatusException(
                    HttpStatus.NOT_FOUND, "Nie znaleziono zasobu", e);
        }
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<Users> getUserById(@PathVariable("id") Integer id){
        try {
            return new ResponseEntity<>(
                    databaseService.getUserById(id),
                    HttpStatus.OK);
        }
        catch(org.springframework.http.converter.HttpMessageNotReadableException e)
        {
            throw new ResponseStatusException(
                    HttpStatus.BAD_REQUEST, "Zawartość nie może być pusta", e);
        }
        catch(java.util.NoSuchElementException e)
        {
            throw new ResponseStatusException(
                    HttpStatus.NOT_FOUND, "Nie znaleziono zasobu", e);
        }
    }
}
