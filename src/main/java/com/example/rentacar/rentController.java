package com.example.rentacar;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.sql.SQLIntegrityConstraintViolationException;
import java.util.Collection;
import java.util.List;

@RestController
@RequestMapping("/rent")
public class rentController {
    @Autowired
    databaseService databaseService;

    @RequestMapping(value = "/", method = RequestMethod.POST, consumes = {"application/json","application/xml"})
    @ResponseBody
    public ResponseEntity<Rent> addRent(@RequestBody Rent rent) {
        try {
            return new ResponseEntity<>(
                    databaseService.insertRent(rent),
                    HttpStatus.OK);
        }
        catch(ResponseStatusException e) {
            if(e.getStatus() == HttpStatus.BAD_REQUEST)
            {
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(databaseService.insertRent(rent));
            }
            if(e.getStatus() == HttpStatus.INTERNAL_SERVER_ERROR)
            {
                return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(databaseService.insertRent(rent));
            }
            else{
                return ResponseEntity.status(e.getStatus()).body(databaseService.insertRent(rent));
            }
        }
        catch(java.util.NoSuchElementException e)
        {
            throw new ResponseStatusException(
                    HttpStatus.NOT_FOUND, "Nie znaleziono zasobu", e);
        }
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.PUT, consumes = {"application/json","application/xml"})
    @ResponseBody
    public ResponseEntity<Rent> updateCar(@PathVariable("id") Integer id, @RequestBody Rent rent) {
        try {
            return new ResponseEntity<>(
                    databaseService.updateRent(id, rent),
                    HttpStatus.OK);
        }
        catch(org.springframework.http.converter.HttpMessageNotReadableException e)
        {
            throw new ResponseStatusException(
                    HttpStatus.BAD_REQUEST, "Zawartość nie może być pusta", e);
        }
        catch(java.util.NoSuchElementException e)
        {
            throw new ResponseStatusException(
                    HttpStatus.NOT_FOUND, "Nie znaleziono zasobu", e);
        }
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    @ResponseBody
    public ResponseEntity<Rent> deleteRent(@PathVariable("id") Integer id) {
        try {
            return new ResponseEntity<>(
                    databaseService.deleteRent(id),
                    HttpStatus.OK);
        }
        catch(org.springframework.http.converter.HttpMessageNotReadableException e)
        {
            throw new ResponseStatusException(
                    HttpStatus.BAD_REQUEST, "Zawartość nie może być pusta", e);
        }
        catch(java.util.NoSuchElementException e)
        {
            throw new ResponseStatusException(
                    HttpStatus.NOT_FOUND, "Nie znaleziono zasobu", e);
        }
    }

    @RequestMapping(value = "/", method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<List<Rent>> getRent(){
        try
        {
            return new ResponseEntity<>(
                    databaseService.getAllRent(),
                    HttpStatus.OK);
        }
        catch(org.springframework.http.converter.HttpMessageNotReadableException e)
        {
            throw new ResponseStatusException(
                    HttpStatus.BAD_REQUEST, "Zawartość nie może być pusta", e);
        }
        catch(java.util.NoSuchElementException e)
        {
            throw new ResponseStatusException(
                    HttpStatus.NOT_FOUND, "Nie znaleziono zasobu", e);
        }
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<Rent> getRentById(@PathVariable("id") Integer id){
        try {
            return new ResponseEntity<>(
                    databaseService.getRentById(id),
                    HttpStatus.OK);
        }
        catch(org.springframework.http.converter.HttpMessageNotReadableException e)
        {
            throw new ResponseStatusException(
                    HttpStatus.BAD_REQUEST, "Zawartość nie może być pusta", e);
        }
        catch(java.util.NoSuchElementException e)
        {
            throw new ResponseStatusException(
                    HttpStatus.NOT_FOUND, "Nie znaleziono zasobu", e);
        }
    }

    @RequestMapping(value = "/serEndRent/{id}", method = RequestMethod.PUT)
    @ResponseBody
    public ResponseEntity<Rent> setEndRent(@PathVariable("id") Integer id){
        try {
            return new ResponseEntity<>(
                    databaseService.setRentEnd(id),
                    HttpStatus.OK);
        }
        catch(org.springframework.http.converter.HttpMessageNotReadableException e)
        {
            throw new ResponseStatusException(
                    HttpStatus.BAD_REQUEST, "Zawartość nie może być pusta", e);
        }
        catch(java.util.NoSuchElementException e)
        {
            throw new ResponseStatusException(
                    HttpStatus.NOT_FOUND, "Nie znaleziono zasobu", e);
        }
    }

    @RequestMapping(value = "/{id_car}/users", method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<Collection<Rent>> getCarsByUsers(@PathVariable("id_car") Integer id_car){
        try {
            return new ResponseEntity<>(
                    databaseService.getCarsByUsers(id_car),
                    HttpStatus.OK);
        }
        catch(org.springframework.http.converter.HttpMessageNotReadableException e)
        {
            throw new ResponseStatusException(
                    HttpStatus.BAD_REQUEST, "Zawartość nie może być pusta", e);
        }
        catch(java.util.NoSuchElementException e)
        {
            throw new ResponseStatusException(
                    HttpStatus.NOT_FOUND, "Nie znaleziono zasobu", e);
        }
    }
}
