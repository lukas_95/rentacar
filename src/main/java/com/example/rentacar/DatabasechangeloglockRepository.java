package com.example.rentacar;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface DatabasechangeloglockRepository extends JpaRepository<Databasechangeloglock, Integer>, JpaSpecificationExecutor<Databasechangeloglock> {

}