package com.example.rentacar;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.HttpMediaTypeNotSupportedException;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

@RestController
@RequestMapping("/cars")
public class carsController {

    @Autowired
    databaseService databaseService;

    @RequestMapping(value = "/", method = RequestMethod.POST, consumes = {"application/json","application/xml"})
    @ResponseBody
    public ResponseEntity<Cars> addCar(@RequestBody Cars cars) {
        try {
            return new ResponseEntity<>(databaseService.insertCars(cars),
                    HttpStatus.OK);
        }
        catch(ResponseStatusException e) {
            if(e.getStatus() == HttpStatus.BAD_REQUEST)
            {
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(databaseService.insertCars(cars));
            }
            if(e.getStatus() == HttpStatus.INTERNAL_SERVER_ERROR)
            {
                return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(databaseService.insertCars(cars));
            }
            else{
                return ResponseEntity.status(e.getStatus()).body(databaseService.insertCars(cars));
            }
        }
        catch(java.util.NoSuchElementException e)
        {
            throw new ResponseStatusException(
                    HttpStatus.NOT_FOUND, "Nie znaleziono zasobu", e);
        }
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.PUT, consumes = {"application/json","application/xml"})
    @ResponseBody
    public ResponseEntity<Cars> updateCar(@PathVariable("id") Integer id, @RequestBody Cars cars) {
        try {
            return new ResponseEntity<>(
                    databaseService.updateCars(id, cars),
                    HttpStatus.OK);
            }
        catch(org.springframework.http.converter.HttpMessageNotReadableException e)
        {
            throw new ResponseStatusException(
                    HttpStatus.BAD_REQUEST, "Zawartość nie może być pusta", e);
        }
        catch(java.util.NoSuchElementException e)
        {
            throw new ResponseStatusException(
                    HttpStatus.NOT_FOUND, "Nie znaleziono zasobu", e);
        }
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    @ResponseBody
    public ResponseEntity<Cars> deleteCar(@PathVariable("id") Integer id) {
        try {
            return new ResponseEntity<>(
                    databaseService.deleteCars(id),
                    HttpStatus.OK);
        }
        catch(org.springframework.http.converter.HttpMessageNotReadableException e)
        {
            throw new ResponseStatusException(
                    HttpStatus.BAD_REQUEST, "Zawartość nie może być pusta", e);
        }
        catch(java.util.NoSuchElementException e)
        {
            throw new ResponseStatusException(
                    HttpStatus.NOT_FOUND, "Nie znaleziono zasobu", e);
        }
    }

    @RequestMapping(value = "/", method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<List<Cars>> getCars(){
       try {
            return new ResponseEntity<>(
                    databaseService.getAllCars(),
                    HttpStatus.OK);
        }
       catch(org.springframework.http.converter.HttpMessageNotReadableException e)
       {
           throw new ResponseStatusException(
                   HttpStatus.BAD_REQUEST, "Zawartość nie może być pusta", e);
       }
       catch(java.util.NoSuchElementException e)
       {
           throw new ResponseStatusException(
                   HttpStatus.NOT_FOUND, "Nie znaleziono zasobu", e);
       }
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<Cars> getCarById(@PathVariable("id") Integer id) {
        try {
            return new ResponseEntity<>(
                    databaseService.getCarById(id),
                    HttpStatus.OK);
        }
        catch(org.springframework.http.converter.HttpMessageNotReadableException e)
        {
            throw new ResponseStatusException(
                    HttpStatus.BAD_REQUEST, "Zawartość nie może być pusta", e);
        }
        catch(java.util.NoSuchElementException e)
        {
            throw new ResponseStatusException(
                    HttpStatus.NOT_FOUND, "Nie znaleziono zasobu", e);
        }
    }
}
