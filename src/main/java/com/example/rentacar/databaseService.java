package com.example.rentacar;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
public class databaseService {
    @Autowired
    CarsRepository carsRepository;
    @Autowired
    RentRepository rentRepository;
    @Autowired
    UsersRepository usersRepository;
    @Autowired
    EmailSendingService emailSendingService;

    public List<Cars> getAllCars()
    {
        List<Cars> all = carsRepository.findAll();
        return all;
    }

    public Cars getCarById(Integer id)
    {
        return carsRepository.findById(id).get();
    }

    public Cars insertCars(Cars cars)
    {
        cars.setIsFree(true);
        carsRepository.save(cars);
        return cars;
    }

    public Cars updateCars(Integer id, Cars cars)
    {
        cars.setId(id);
        carsRepository.save(cars);
        
        return cars;
    }

    public Cars deleteCars(Integer id)
    {
        Optional<Cars> car = carsRepository.findById(id);
        carsRepository.deleteById(id);
        return car.get();
    }

    public List<Users> getAllUsers()
    {
        List<Users> all = usersRepository.findAll();

        return all;
    }

    public Users getUserById(Integer id)
    {
        return usersRepository.findById(id).get();
    }

    public Users insertUsers(Users users)
    {
        usersRepository.save(users);
        return users;

    }

    public Users updateUsers(Integer id, Users users)
    {
        users.setId(id);
        usersRepository.save(users);
        return users;
    }

    public Users deleteUsers(Integer id)
    {
        Optional<Users> user = usersRepository.findById(id);
        usersRepository.deleteById(id);
        return user.get();
    }

    public List<Rent> getAllRent()
    {
        List<Rent> all = rentRepository.findAll();

        return all;
    }

    public Rent getRentById(Integer id)
    {
       return rentRepository.findById(id).get();
    }

    public Rent insertRent(Rent rent)
    {
        long diff = rent.getEndDate().getTime() - rent.getStartDate().getTime();
        float days = (diff / (1000*60*60*24));
        Optional<Cars> car = carsRepository.findById(rent.getIdCars().getId());
        Optional<Users> user = usersRepository.findById(rent.getIdUser().getId());
        rent.setPrice(days*car.get().getCostPerDay());
        rent.setIsBack(false);
        car.get().setIsFree(false);
        rentRepository.save(rent);
        emailSendingService.sender(rent,car.get(),user.get());

        return rent;
    }

    public Rent updateRent(Integer id, Rent rent)
    {
        rent.setId(id);

        rentRepository.save(rent);

        return rent;
    }

    public Rent deleteRent(Integer id)
    {
        Optional<Rent> rent = rentRepository.findById(id);
        rentRepository.deleteById(id);
        return rent.get();
    }

    public Rent setRentEnd(Integer id)
    {
        Rent rent = rentRepository.findAllById(id);
        rent.setIsBack(true);
        Cars car = rent.getIdCars();
        car.setIsFree(true);
        rentRepository.save(rent);
        carsRepository.save(car);

        return rent;
    }

    public Collection<Rent> getCarsByUsers(Integer id_car) {
        Optional<Users> user = usersRepository.findById(id_car);
        return user.get().getRentCollection();
    }

    public void longRent() {
        Date currentDate = new Date();
        List<Rent> all = rentRepository.findAll();
        for(Rent rent:all) {
            if(currentDate.after(rent.getEndDate()))
            {
                rent.setPrice(rent.getPrice() + 100.00);
                rentRepository.save(rent);
            }
        }
    }
}
