package com.example.rentacar;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import java.util.List;
import java.util.Optional;

public interface RentRepository extends JpaRepository<Rent, Integer>, JpaSpecificationExecutor<Rent> {

    Rent findAllById(Integer id);
    Rent findByIdUser(Users users);
    List<Rent> findUsersByIdCars(Cars cars);

}