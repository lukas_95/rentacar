package com.example.rentacar;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;

import java.util.Timer;
import java.util.TimerTask;

@SpringBootApplication
public class RentacarApplication {

	static databaseService databaseService;

	@Autowired
	RentacarApplication(databaseService databaseService)
	{
		RentacarApplication.databaseService = databaseService;
	}

	public static void main(String[] args) {
		SpringApplication.run(RentacarApplication.class, args);
		Timer timer = new Timer();
		timer.schedule(new TimerTask() {
			@Override
			public void run() {
				databaseService.longRent();
			}
		}, 0, 1000 * 60 * 5);
	}

}
