package com.example.rentacar;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import java.util.List;

public interface CarsRepository extends JpaRepository<Cars, Integer>, JpaSpecificationExecutor<Cars> {
}