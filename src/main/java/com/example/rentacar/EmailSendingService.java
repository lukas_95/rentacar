package com.example.rentacar;

import org.springframework.context.annotation.Bean;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Properties;

@Service
public class EmailSendingService {
    @Bean
    public JavaMailSender getJavaMailSender() {
        JavaMailSenderImpl mailSender = new JavaMailSenderImpl();
        mailSender.setHost("smtp.gmail.com");
        mailSender.setPort(587);

        mailSender.setUsername("projekt.rent.a.car@gmail.com");
        mailSender.setPassword("@Ndrzej12");

        Properties props = mailSender.getJavaMailProperties();
        props.put("mail.transport.protocol", "smtp");
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.debug", "true");
        props.put("mail.smtp.ssl.trust", "smtp.gmail.com");

        return mailSender;
    }

    public void sender(Rent rent, Cars car, Users user) {
                SimpleMailMessage message = new SimpleMailMessage();
                message.setTo(user.getEmail());
                message.setSubject("Rezerwacja nr" + rent.getId());
                message.setText("Gratulecje. Właśnie zarezerwowałeś samochód: \n" + "Producent: "  + car.getManufacturer() + "\n" + "Model: " + car.getModel() + "\n" + "Koszt: " + rent.getPrice());
                getJavaMailSender().send(message);
    }
}
